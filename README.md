# README #

wbpage: http://jungletronics.blogspot.com.br/2014/06/blog-post.html

Anounce V.0.3!!!!!

see the proof of concept: 3 archieves morning, clouds and sunset_youtube_64x.mp4 (downloads)
read WIKI!
Date: 22/jun/2014

Hello!!! 
![bitPhoto.jpg](https://bitbucket.org/repo/qAMk4A/images/580143102-bitPhoto.jpg)
Welcome to **JR TimeLapse Cam Project**! The camera lets you take pictures at predetermined intervals. The photo is sent via the serial port to the sdcard in ddmmhhmm.png format. as the transfer is slow, 8300 bits / sec, the interval is the sum of 6000 milliseconds and the time for return of GetData() method, which takes approximately 30000 milliseconds; total take-up picture every 36-45 seconds. I want to give the possibility to set the time without having to compile the code and add the OLED component. Preliminary tests made ​​with all components and codes are also available. Watch movie (clouds_youtube_64x.mp4) available on downloads produced with photos with **JR TimeLapse Cam Project**. Suggestions are welcome on WIKI  page!!!
Thank you!!! Thank you!!!

FILE: **GroveSerialCamera.ino** 

HARDWARE:
 **Pro Micro - 5V/16MHz** - https://www.sparkfun.com/products/12640
 **Grove - Serial Camera Kit** -  http://www.seeedstudio.com/depot/Grove-Serial-Camera-Kit-p-1608.html?cPath=25_33
 **DS1307 Real Time Clock breakout board kit** - https://www.adafruit.com/products/264
 **LC Studio SD card breakout board**  -  http://www.ebay.com/itm/SD-Card-Module-Slot-Socket-Reader-LC-Studio-3-3V-5V-For-Arduino-ARM-MCU-/300956938609
  
CAMERA SPECIFICATIONS:
    **Input Voltage**: 5V
    **Pixel**: 300,000
    **Resolution**: 640*480, 320*240, 160*120
    **Uart Baud Rate**: 9600~115200
    **Communication**: RS485 and RS232
    Photo JPEG compression, high, medium and low grades Optional
    AGC
    Auto Exposure Event Control
    Automatic White Balance Control
    Focus adjustable 

REFERENCES:
 https://github.com/Seeed-Studio/Grove_Serial_Camera_Kit
 http://www.seeedstudio.com/wiki/Grove_-_Serial_Camera_Kit
 http://www.google.com.br/url?sa=t&rct=j&q=&esrc=s&source=web&cd=3&ved=0CC0QFjAC&url=http%3A%2F%2Fwww.element14.com%2Fcommunity%2Fservlet%2FJiveServlet%2Fdownload%2F81339-115127%2FCJ-OV528%2520Protocol%25EF%25BC%2588%25E9%2580%259A%25E4%25BF%25A1%25E5%258D%258F%25E8%25AE%25AE%25EF%25BC%2589.pdf&ei=8zWiU4GaB7OqsQS0loDgDA&usg=AFQjCNG9CITsABenZ7giPdVwrkA5IFvjNg&sig2=Al1sACCZ5S7FB4keLzD7sQ&bvm=bv.69411363,d.cWc&cad=rja
 
giljr.2009@gmail.com
Please read LICENSE.